# ShopManager
Sample warehouse management project
 
 ![alt text](src/main/resources/icons/readmepicture.png)

## Installation

Project written in Java 11. JavaFx jar files are included in the sources.
Sometimes there is a need to add --module-path <your path to jars> --add-modules=javafx.controls,javafx.fxml
in the vm options in order to run it. 


```bash
git clone https://mmihuu@bitbucket.org/mmihuu/shoe-shop-manager.git
```

## Technologies

* java 11
* Scene Builder
* javaFX
* SQLite
* JUnit 5
