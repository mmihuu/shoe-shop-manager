package main.tools;

import com.sun.scenario.effect.impl.sw.java.JSWBlend_SRC_OUTPeer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.ls.LSOutput;

public class IntegerTextField extends TextField {

    private static final Logger logger = LoggerFactory.getLogger(IntegerTextField.class);

    public IntegerTextField() {
        super();

        addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {


            public void handle(KeyEvent event) {
                System.out.println("sprawdzam");
                if (!isValid(getText())) {

                    event.consume();
                }
            }
        });

        textProperty().addListener(new ChangeListener<String>() {

            public void changed(ObservableValue<? extends String> observableValue,
                                String oldValue, String newValue) {
                System.out.println("sprawdzam");
                if(!isValid(newValue)) {
                    setText(oldValue);
                }
            }
        });
    }

    private boolean isValid(final String value) {
        if (value.length() == 0 || value.equals("-")) {
            return true;
        }

        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public int getInt() {
        try {
            return Integer.parseInt(getText());
        }
        catch (NumberFormatException e) {
            logger.error("Error parsing int (" + getText() +") from field.", e);
            return 0;
        }
    }
}
