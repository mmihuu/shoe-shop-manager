package main.tools;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import javafx.collections.ObservableList;
import main.shoe.CountByTheSize;
import main.shoe.ListOfSizes;
import main.shoe.Product;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;


public class InvertoryDoPDF {


    private final static String[] HEADER = {"nazwa", "typ", "cena za sztukę", "dostepne rozmiary", "kolekcja", "kolor", "wartość rynkowa", "sztuk w kartonie"};


    private final static Font SMALL_BOLD = new Font(Font.FontFamily.TIMES_ROMAN, 8,
            Font.BOLD);
    private final static Font NORMAL_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 8,
            Font.NORMAL);
    private final static Font LARGE_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.NORMAL);

    public static void addMetaData(Document document, String sqlXMLFileName) {


    }

    public static void addContent(Document document, ObservableList<Product> map, List<ListOfSizes> listOfSizes, List<CountByTheSize> listCountByTheSize) throws DocumentException {
        Paragraph paragraph = new Paragraph();
        paragraph.setFont(NORMAL_FONT);
        createReportTable(paragraph, map, listOfSizes, listCountByTheSize);
        document.add(paragraph);
    }

    private static void createReportTable(Paragraph paragraph, ObservableList<Product> listOfPairOfShoes
            , List<ListOfSizes> listOfSizes, List<CountByTheSize> listCountByTheSize)
            throws BadElementException {


        PdfPTable table = new PdfPTable(8);

        table.setWidthPercentage(100);
        paragraph.add(new Chunk("", SMALL_BOLD));
        if (listOfPairOfShoes.isEmpty()) {
            paragraph.add(new Chunk("nie ma butów w magazynie"));
            return;
        }
        addHeaderInTable(table);


        for (Product listOfPairOfShoe : listOfPairOfShoes) {

            addToTable(table, listOfPairOfShoe.getNameOfShoes());
            addToTable(table, listOfPairOfShoe.getTypeOfShoes());
            addToTable(table, String.valueOf(listOfPairOfShoe.getPrice()));
            /*addToTable(table, setPDf(listOfPairOfShoes.get(i),listOfSizes,listCountByTheSize));*/
            addToTable(table, String.valueOf(listOfPairOfShoe.getCollectionOfShoes()));
            addToTable(table, listOfPairOfShoe.getColorOfShoes());
            /*addToTable(table, String.valueOf(((S) listOfPairOfShoes.get(i)).getCount()));
            addToTable(table, String.valueOf(((ShoesWareHouse) listOfPairOfShoes.get(i)).getHowManyShoes()));*/

        }

        paragraph.add(table);
    }

    /**
     * Helper methods start here
     **/
    public static void addTitlePage(Document document, String title, LocalDate start) throws DocumentException {

        Paragraph preface = new Paragraph();
        preface.add(new Phrase(title, InvertoryDoPDF.LARGE_FONT));
        addEmptyLine(preface);
        preface.add(new Phrase("Stan magazynu na  " + start.toString(), SMALL_BOLD));
        /*preface.add(new Phrase(new Date().toString(), NORMAL_FONT));*/

        document.addSubject("PDF : " + title);
        preface.setAlignment(Element.ALIGN_CENTER);
        document.add(preface);

    }

    private static void addEmptyLine(Paragraph paragraph) {
        for (int i = 0; i < 1; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static void addHeaderInTable(PdfPTable table) {
        PdfPCell c1;
        for (String header : InvertoryDoPDF.HEADER) {
            c1 = new PdfPCell(new Phrase(header, new Font(SMALL_BOLD)));
            c1.setBackgroundColor(BaseColor.GREEN);
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
        }
        table.setHeaderRows(1);
    }

    private static void addToTable(PdfPTable table, String data) {

        BaseFont bf1 = null;

        {
            try {
                bf1 = BaseFont.createFont("c:/windows/fonts/arial.ttf",
                        BaseFont.CP1250, BaseFont.CACHED);
            } catch (DocumentException | IOException e) {
                e.printStackTrace();
            }
        }

        Font polskieFonty = new Font(bf1, 12);


       /* Font f = new Font(bf1, 12);*/

        table.addCell(new Phrase(data, polskieFonty));

    }


    private static String setPDf(Product o, List<ListOfSizes> listOfSizes, List<CountByTheSize> countByTheSizes) {

        StringBuilder str = new StringBuilder();


        for (int i = 0; i < countByTheSizes.size(); i++) {

                System.out.println(o.getTypeOfShoes());
                System.out.println(countByTheSizes.get(i).getTypeOfShoes());
                if (o.getNameOfShoes().equals(countByTheSizes.get(i).getTypeOfShoes())) {
                    str.append(listOfSizes.get(i).getSizeOfShoes()).append(" -->").append(countByTheSizes.get(i).getCount()).append("\n");


                }
            }


            return str.toString();

    }
}




