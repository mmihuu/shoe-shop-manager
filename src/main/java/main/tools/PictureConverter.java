package main.tools;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import main.shoe.Product;


import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PictureConverter {

    private static byte[] imageInByte;

    public static byte[] toByteArray(java.io.File img) {


        BufferedImage originalImage = null;
        try {
            originalImage = ImageIO.read(img);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(originalImage, "jpg", baos);
            imageInByte = baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return imageInByte;


    }

    public static WritableImage toImage(byte[] b) {

        BufferedImage img = null;
        try {
            img = ImageIO.read(new ByteArrayInputStream(b));
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(b.length+ " size in toImage");

        WritableImage writableImage = SwingFXUtils.toFXImage(img, null);
        return writableImage;

    }






}
