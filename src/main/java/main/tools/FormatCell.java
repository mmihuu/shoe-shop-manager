package main.tools;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import main.shoe.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class FormatCell {


    static double height = 0.0;

    private ObservableList<Product> oList = FXCollections.observableArrayList();

    private VBox mainDisplay = new VBox();

    //values for width in displaywindow (distance beteen different objects in vbox)
    private List<Integer> endPos = Arrays.asList(287, 360, 428, 503, 550);
    private List<Integer> posToSub = Arrays.asList(207, 287, 360, 428, 503);

    TextFlow flow;
    HBox hBoxS;

    public List<HBox> checkManyRecords(Map<String, String> m) {

        return createCells(ProductService.checkMultipleQueries(m));

    }

    public List<HBox> selectAll() {

       /* ReadRecord readRecord = new ReadRecord();
        readRecord.selectAll();
        oList = readRecord.getoList();

        return createCells(oList);*/
        ObservableList<Product> products = ProductService.getProducts();

        System.out.println(products.size());
        return createCells(products);

    }
    public List<HBox> getQueryFromTxBox(ObservableList<Product> o) {


        return createCells(o);

    }

    public List<HBox> selectSold(String timeback) {


        /*readRecord.selectForReportBetweenDates(timeback);*/


    /*    return createCellsForHistory(readRecord.getoList());
*/
    List<HBox> lH = new ArrayList<>();
    return lH;
    }





    public List<HBox> createCells(ObservableList<Product> o) {


        List<HBox> listOfXbox = new ArrayList<>();




        for (Product a : o) {

            HBox hBox = getHBox();
            CheckBox checkBox = new CheckBox();
            checkBox.setOnAction(actionEvent -> {


                        ProductService.insertToSellTable(new SoldProduct(a));

                        ProductService.removeById(a.getId());





                        mainDisplay.getChildren().addAll(hBox);


                    }
            );

            Pane first = new Pane();
            first.setMinHeight(5);
            first.setMinWidth(7);


            hBox.getChildren().addAll(getImage(PictureConverter.toImage(a.getImgOfShoes())),first,
                    getLabel(a.getTypeOfShoes()), setTheGapInHbox(endPos.get(0), posToSub.get(0), a.getTypeOfShoes())
                    , getLabel(a.getSizeOfShoes()), setTheGapInHbox(endPos.get(1), posToSub.get(1), a.getSizeOfShoes()),
                    getLabel("jesienna"), setTheGapInHbox(endPos.get(2), posToSub.get(2), CollectionOfShoes.jesienna.name()),
                    getLabel(a.getColorOfShoes()), setTheGapInHbox(endPos.get(3), posToSub.get(3), a.getColorOfShoes()), getLabel(a.getPrice()),
                    setTheGapInHbox(endPos.get(4), posToSub.get(4)), checkBox);


            listOfXbox.add(hBox);
            mainDisplay.getChildren().addAll(hBox);


        }


        return listOfXbox;


    }

    public List<HBox> createCellsForHistory(ObservableList<SoldProduct> o) {


        List<HBox> listOfXbox = new ArrayList<>();



        for (SoldProduct a : o) {
            Label l = new Label();
           /* l.minHeight(30);*/

            l.setPrefWidth(55);

            SoldProduct a1 =  a;

            HBox hBox = getHBox();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.forLanguageTag("pl"));

            try {

                flow = new TextFlow();
                Date date = sdf.parse( a.getLocalDateTime().toString());
                String formatDate = sdf.format(date);



                l.minHeight(60);

                l.maxWidth(5);
                l.setWrapText(true);
                l.setText(formatDate);
                l.setFont(Font.font(10));

                flow.getChildren().add(l);





                System.out.println(l.getText());

            } catch (ParseException e) {
                e.printStackTrace();
            }


           /* l.setText(((PairOfShoesHistory) a).getDate());
            l.setWrapText(true);*/


            Pane first = new Pane();
            first.setMinHeight(5);
            first.setMinWidth(7);



            hBox.getChildren().addAll(getImage(PictureConverter.toImage(a.getImgOfShoes())), first,
                    getLabel(a.getTypeOfShoes()), setTheGapInHbox(endPos.get(0), posToSub.get(0), a.getTypeOfShoes())
                    , getLabel(a.getSizeOfShoes()), setTheGapInHbox(endPos.get(1), posToSub.get(1), a.getSizeOfShoes()),
                    getLabel(a.getCollectionOfShoes()), setTheGapInHbox(endPos.get(2), posToSub.get(2), CollectionOfShoes.jesienna.name()),
                    getLabel(a.getColorOfShoes()), setTheGapInHbox(endPos.get(3), posToSub.get(3), a.getColorOfShoes()),
                    setTheGapInHbox(endPos.get(4), posToSub.get(4)), setTheGapInHbox(45), l );



            listOfXbox.add(hBox);
            /*mainDisplay.getChildren().addAll(hBox);*/


        }
        System.out.println(listOfXbox.size()+" Wielkość tej listy");


        return listOfXbox;


    }

    public List<HBox> createCellsForWare(ObservableList<Product> o, List<ListOfSizes> listOfSizes, List<CountByTheSize> countByTheSizes) {


        List<HBox> listOfXbox = new ArrayList<>();





        for (Product a : o) {


            List<Integer> listka = new ArrayList<>();
            ScrollPane scrollPane = new ScrollPane();



            Pane p = new Pane();

            p.setMinHeight(20);



            VBox vi = new VBox();
            vi.getChildren().add(p);


            for (int i = 0; i < countByTheSizes.size(); i++) {

                System.out.println(a.getTypeOfShoes());
                System.out.println(countByTheSizes.get(i).getTypeOfShoes());
                if (a.getNameOfShoes().equals(countByTheSizes.get(i).getTypeOfShoes())) {

                   /* System.out.println("prawda");
                    listka.add(countByTheSizes.get(i).getSizeOfShoes());*/

                    Label size = new Label();


                    size.setText(String.valueOf(listOfSizes.get(i).getSizeOfShoes())+" -->"+countByTheSizes.get(i).getCount());
                    Label howManyModels = new Label();

                    vi.getChildren().add(size);
                    vi.maxWidth(30);


                }
            }

            scrollPane.setContent(vi);

            Label count = new Label();



            HBox hBox = getHBox();


            Pane first = new Pane();
            first.setMinHeight(5);
            first.setMinWidth(7);





            scrollPane.setMinWidth(50);
            scrollPane.setMaxWidth(60);

            /*hBox.getChildren().addAll(getImage(getImage(PictureConverter.toImage(a.getImgOfShoes())), first,
                    getLabel(a.getTypeOfShoes()), setTheGapInHbox(endPos.get(0), posToSub.get(0), a.getTypeOfShoes())
                    , scrollPane, setTheGapInHbox(endPos.get(1), posToSub.get(1), a.getSizeOfShoes()),
                    getLabel(a.getCollectionOfShoes()), setTheGapInHbox(endPos.get(2), posToSub.get(2), "jesienne"),
                    getLabel(a.getColorOfShoes()), setTheGapInHbox(endPos.get(3), posToSub.get(3), a.getColorOfShoes())));*/


            listOfXbox.add(hBox);
            mainDisplay.getChildren().addAll(hBox);


        }


        return listOfXbox;


    }

    //methods to create a dynamically populated iteams in wearhouse
    private HBox getHBox() {

        HBox hBox = new HBox();

        hBox.setLayoutX(0.0);
        hBox.setLayoutY(height);
        hBox.setMaxHeight(147);
        hBox.setMaxWidth(700);
        hBox.setStyle("-fx-border-color: #1eb3ab;");
        hBox.setAlignment(Pos.CENTER_LEFT);
        height = height + 162;

        return hBox;
    }


    private ImageView getImage(Image img) {

        ImageView imageView = new ImageView();
        imageView.setFitHeight(200);
        imageView.setFitWidth(200);
        imageView.setY(0);
        imageView.setPreserveRatio(true);
        imageView.setImage(img);

        return imageView;

    }

    private Label getLabel(Object o) {

        Label t = new Label();
        t.setText(o.toString());
        t.setFont(Font.font(18));


        return t;
    }

    private Pane setTheGapInHbox(double distance, double startPos, Object a) {

        Pane pc = new Pane();
        pc.setMinHeight(5);
        double minWidthColors = distance - (startPos + sizeOfLabel(a));
        pc.setMinWidth(minWidthColors);
        return pc;

    }



    private Pane setTheGapInHbox(double distance, double startPos) {

        Pane pc = new Pane();
        pc.setMinHeight(5);
        double minWidthColors = distance - (startPos);
        pc.setMinWidth(minWidthColors);
        return pc;

    }

    private Pane setTheGapInHbox(double width) {

        Pane pc = new Pane();
        pc.setMinHeight(5);

        pc.setMinWidth(width);
        return pc;

    }

    private double sizeOfLabel(Object a) {

        if (a instanceof Integer)
            a = a.toString();

        Text t = new Text((String) a);
        t.setFont(Font.font(18));
        new Scene(new Group(t));
        t.applyCss();
        double widthPrice = t.getLayoutBounds().getWidth();

        return widthPrice;

    }





    private List<HBox> getPrice(String p) {

        return createCells(oList);
    }


}
