package main.arduinocontroller;

import org.sintef.jarduino.DigitalPin;
import org.sintef.jarduino.JArduino;
import org.sintef.jarduino.PinMode;
import org.sintef.jarduino.comm.Serial4JArduino;

import static org.sintef.jarduino.DigitalState.HIGH;
import static org.sintef.jarduino.DigitalState.LOW;

public class StepperMotor extends JArduino {



    int step_number = 0;

    public StepperMotor(String port) {
        super(port);
    }

    @Override
    protected void setup() {
        pinMode(DigitalPin.PIN_8, PinMode.OUTPUT);
        pinMode(DigitalPin.PIN_10, PinMode.OUTPUT);
        pinMode(DigitalPin.PIN_11, PinMode.OUTPUT);
        pinMode(DigitalPin.PIN_12, PinMode.OUTPUT);

    }

    @Override
    protected void loop() {
        OneStep(false);
        delay(2);


    }


    void OneStep(boolean dir){
        if(dir){
            switch(step_number){
                case 0:
                    digitalWrite(DigitalPin.PIN_8, HIGH);
                    digitalWrite(DigitalPin.PIN_10, LOW);
                    digitalWrite(DigitalPin.PIN_11, LOW);
                    digitalWrite(DigitalPin.PIN_12, LOW);
                    break;
                case 1:
                    digitalWrite(DigitalPin.PIN_8,LOW);
                    digitalWrite(DigitalPin.PIN_10, HIGH);
                    digitalWrite(DigitalPin.PIN_11, LOW);
                    digitalWrite(DigitalPin.PIN_12, LOW);
                    break;
                case 2:
                    digitalWrite(DigitalPin.PIN_8,LOW);
                    digitalWrite(DigitalPin.PIN_10, LOW);
                    digitalWrite(DigitalPin.PIN_11, HIGH);
                    digitalWrite(DigitalPin.PIN_12, LOW);
                    break;
                case 3:
                    digitalWrite(DigitalPin.PIN_8,LOW);
                    digitalWrite(DigitalPin.PIN_10, LOW);
                    digitalWrite(DigitalPin.PIN_11, LOW);
                    digitalWrite(DigitalPin.PIN_12, HIGH);
                    break;
            }
        }else{
            switch(step_number){
                case 0:
                    digitalWrite(DigitalPin.PIN_8,LOW);
                    digitalWrite(DigitalPin.PIN_10, LOW);
                    digitalWrite(DigitalPin.PIN_11, LOW);
                    digitalWrite(DigitalPin.PIN_12, HIGH);
                    break;
                case 1:
                    digitalWrite(DigitalPin.PIN_8,LOW);
                    digitalWrite(DigitalPin.PIN_10, LOW);
                    digitalWrite(DigitalPin.PIN_11, HIGH);
                    digitalWrite(DigitalPin.PIN_12, LOW);
                    break;
                case 2:
                    digitalWrite(DigitalPin.PIN_8,LOW);
                    digitalWrite(DigitalPin.PIN_10, HIGH);
                    digitalWrite(DigitalPin.PIN_11, LOW);
                    digitalWrite(DigitalPin.PIN_12, LOW);
                    break;
                case 3:
                    digitalWrite(DigitalPin.PIN_8,HIGH);
                    digitalWrite(DigitalPin.PIN_10, LOW);
                    digitalWrite(DigitalPin.PIN_11, LOW);
                    digitalWrite(DigitalPin.PIN_12, LOW);


            }
        }
        step_number++;
        if(step_number > 3){
            step_number = 0;
        }
    }

    public static void main(String[] args) {
        String serialPort;
        if (args.length == 1) {
            serialPort = args[0];
        } else {
            serialPort = Serial4JArduino.selectSerialPort();
        }
        JArduino arduino = new StepperMotor(serialPort);
        arduino.runArduinoProcess();
    }




}
