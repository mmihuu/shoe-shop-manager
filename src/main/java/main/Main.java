package main;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


public class Main extends Application {


    private final static Logger logger = LoggerFactory.getLogger(Main.class);


    public static void main(String[] args) throws IOException {

        launch(args);


    }


    @Override
    public void start(Stage stage) throws Exception {

        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/mainwindow.fxml"));


        Scene scene = new Scene(loader.load());
        stage.getIcons().add(new Image(Main.class.getResourceAsStream("/icons/icons8_shop_64px.png")));

        stage.setScene(scene);
        stage.show();
    }
}
