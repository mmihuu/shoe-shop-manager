package main.controller.report;

import javafx.collections.ObservableList;
import javafx.scene.chart.*;
import javafx.scene.layout.VBox;
import main.shoe.ProductService;
import main.shoe.SoldProduct;

import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class GenerateReport {

    private BarChart<Double,String> barChart;

    private VBox mainDisplay = new VBox();




    public VBox getLineChart(ObservableList<SoldProduct> pairOfShoes) {


        Map<String, Double> dailySell = getDailySell(pairOfShoes);


        System.out.println(dailySell);

        CategoryAxis categoryAxis = new CategoryAxis();
        NumberAxis numberAxis = new NumberAxis();


        LineChart<Double, String> lineChart = new LineChart(categoryAxis, numberAxis);

        lineChart.setTitle("Wykres sprzedaży");


        XYChart.Series<Double, String> xyChart = new XYChart.Series<>();

        for (Map.Entry<String, Double> entry : dailySell.entrySet()) {
            String tmpString = entry.getKey();
            System.out.println(entry.getValue());

            xyChart.getData().addAll(new XYChart.Data(tmpString, entry.getValue()));



        }

        lineChart.getData().addAll(xyChart);

        mainDisplay.getChildren().addAll(lineChart);

        return mainDisplay;
    }

    public VBox getBarChart(ObservableList<SoldProduct> soldProduct){



        Map<String, Double> dailySell = getDailySell(soldProduct);


        CategoryAxis categoryAxis = new CategoryAxis();
        NumberAxis numberAxis = new NumberAxis();


        barChart = new BarChart(categoryAxis,numberAxis);
        barChart.setTitle("Wykres sprzedaży");

        XYChart.Series<Double, String> xyChart = new XYChart.Series();

        for (Map.Entry<String, Double> entry : dailySell.entrySet()) {
            String tmpString = entry.getKey();
            System.out.println(entry.getValue());

            xyChart.getData().addAll(new XYChart.Data(tmpString, entry.getValue()));


        }


        barChart.getData().addAll(xyChart);
        mainDisplay.getChildren().addAll(barChart);




        return mainDisplay;

    }



    public Map<String, Double> getDailySell(ObservableList<SoldProduct> e) {

        Map<String, Double> groupByDate = (Map<String, Double>) e.stream()
                .collect(Collectors.groupingBy(SoldProduct::getDay, Collectors.summingDouble(SoldProduct::getPrice)));

        Map<String, Double> m = new TreeMap<>(groupByDate);


        return m;
    }


}
