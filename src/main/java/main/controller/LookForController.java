package main.controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class LookForController {

    @FXML
    public Button goBack;
    @FXML
    public VBox leftVbox;
    @FXML
    public TextField lookForPrice;
    @FXML
    public TextField lookForSize;
    @FXML
    public TextField lookForName;
    @FXML
    public TextField lookForType;

    Map<String, String> mapValuesDB = new TreeMap<>();


    private String priceToMainWindow;

    public void lookFor(ActionEvent actionEvent) {



    }

    public void goBackToMainWindow(ActionEvent actionEvent) throws IOException {


        priceToMainWindow=lookForPrice.getText()+lookForType.getText();

        if(lookForPrice.getText().length()>0)
            System.out.println(mapValuesDB.put("PRICE ",  lookForPrice.getText()));
        if(lookForType.getText().length()>0)
            System.out.println(mapValuesDB.put("TYPE_OF_SHOES","'"+ lookForType.getText()+"'"));
        if(lookForName.getText().length()>0)
            System.out.println(mapValuesDB.put("NAME_OF_SHOES","'"+ lookForName.getText()+"'"));
        if(lookForSize.getText().length()>0)
            mapValuesDB.put("SIZE_OF_SHOES ", lookForSize.getText());


        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/mainwindow.fxml"));
        Parent pane = null;

        leftVbox.getScene().setRoot(loader.load());

        if(mapValuesDB.size()!=0) {
            MainWindow mainWindow = loader.getController();
            mainWindow.initData(lookForPrice.getText(), mapValuesDB);
            System.out.println(mapValuesDB);
        }
        else {

            MainWindow mainWindow = loader.getController();
            mainWindow.initDataAll();



        }





    }

    public void lookForPriceAction(ActionEvent actionEvent) {

        System.out.println(mapValuesDB.put("price", lookForPrice.getText()));
        System.out.println(mapValuesDB);


    }

    public void lookForSizeAction(ActionEvent actionEvent) {
    }

    public void lookForNameAction(ActionEvent actionEvent) {

    }

    public void lookForTypeAction(ActionEvent actionEvent) {

        System.out.println(mapValuesDB);


    }



    //gettery settery


    public TextField getLookForPrice() {
        return lookForPrice;
    }

    public void setLookForPrice(TextField lookForPrice) {
        this.lookForPrice = lookForPrice;
    }

    public String getPriceToMainWindow() {
        return priceToMainWindow;
    }

    public void setPriceToMainWindow(String priceToMainWindow) {
        this.priceToMainWindow = priceToMainWindow;
    }
}
