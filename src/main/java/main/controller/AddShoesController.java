package main.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.shoe.CollectionOfShoes;
import main.shoe.Product;
import main.shoe.ProductService;
import main.tools.PictureConverter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AddShoesController {

    @FXML public ImageView ImageToDB;
    @FXML public TextField typShoes;
    @FXML public TextField ColorShoes;
    @FXML public TextField sizeShoes;
    @FXML public TextField price;
    @FXML public TextField nameShoes;


    @FXML public ToggleButton d24;
    @FXML public ToggleButton d25;
    @FXML public ToggleButton d26;
    @FXML public ToggleButton d27;
    @FXML public ComboBox collectionShoesCombo;

    public List listOfShoesSizes = new ArrayList();


    File imageInFile;



    public void addPicture(MouseEvent mouseEvent) {

        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files",
                        "*.bmp", "*.png", "*.jpg", "*.gif"));
        chooser.setTitle("Open File");
        imageInFile = chooser.showOpenDialog(new Stage());
        if (imageInFile != null) {
            String imagepath = imageInFile.toURI().toString();
            System.out.println("file:" + imagepath);
            Image image = new Image(imagepath);
            ImageToDB.setImage(image);
        } else {

            infoBox("Proszę wybrać zdjęcie. Akceptowane formaty: png, jpg, bmp.", "Uwaga");


        }

    }

    public void addRecordToDB() throws IOException {


        for(int i = 0; i < listOfShoesSizes.size(); i++) {
            Product p = new Product.Builder().nameOfShoes(nameShoes.getText())
                    .typeOfShoes(typShoes.getText())
                    .priceOfShoes(Float.parseFloat(price.getText()))
                    .colorOfShoes(ColorShoes.getText())
                    .imgOfShoes(PictureConverter.toByteArray(imageInFile))
                    .sizeOfShoes((Integer) listOfShoesSizes.get(i))
                    .collectionOfShoes("jesienna")
                    .build();

            ProductService.save(p);
        }



    }

    public void confirm(ActionEvent actionEvent) throws IOException {


        System.out.println(price.getText() + " to jest ta wartość");
        addRecordToDB();


    }

    @FXML
    public void initialize() {

        collectionShoesCombo.setItems(FXCollections.observableArrayList(CollectionOfShoes.values()));





        price.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("^[0-9A-F]+\\.?[0-9A-F]*")) {
                    infoBox("Proszę wpisać liczby, wartość dziesiętną oddzielamy kropką", "Uwaga");

                    price.setText(newValue.replaceAll("[^\\d.]", ""));
                }

            }
        });




    }





    // ustawianie rozmiarów w kartonie i infoboxy
    public void onAction24(ActionEvent actionEvent) {


        int size24 = 24;
        listOfShoesSizes.add(size24);


    }

    public void onAction25(ActionEvent actionEvent) {

        int size25 = 25;
        listOfShoesSizes.add(size25);
        System.out.println("dodany");
        System.out.println(listOfShoesSizes);

    }

    public void onAction26(ActionEvent actionEvent) {

        int size26 = 26;
        listOfShoesSizes.add(size26);

    }

    public void onAction27(ActionEvent actionEvent) {

        int size27 = 27;
        listOfShoesSizes.add(size27);

    }

    public void onActionSize(ActionEvent actionEvent) {


        if (actionEvent.getSource() == d24) {


            if (d24.isSelected()) {

                listOfShoesSizes.add(24);

            }
            if (!d24.isSelected()) {
                Integer d24toRemove = 24;
                listOfShoesSizes.remove(d24toRemove);

            }
        }
        if (actionEvent.getSource() == d25) {

            if (d25.isSelected()) {

                listOfShoesSizes.add(25);

            }
            if (!d25.isSelected()) {
                Integer d25toRemove = 25;
                listOfShoesSizes.remove(d25toRemove);

            }
        }
        if (actionEvent.getSource() == d26) {
            if (d26.isSelected()) {
                listOfShoesSizes.add(26);

            }
            if (!d26.isSelected()) {
                Integer d26toRemove = 26;
                listOfShoesSizes.remove(d26toRemove);

            }
        }


        if (actionEvent.getSource() == d27) {
            if (d27.isSelected()) {

                listOfShoesSizes.add(27);

            }
            if (!d27.isSelected()) {
                Integer d27toRemove = 27;
                listOfShoesSizes.remove(d27toRemove);

            }
        }


    }



    // ================= info boxy==================

    public static void infoBox(String infoMessage, String titleBar) {
        /* By specifying a null headerMessage String, we cause the dialog to
           not have a header */
        infoBox(infoMessage, titleBar, null);
    }

    public static void infoBox(String infoMessage, String titleBar, String headerMessage) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titleBar);
        alert.setHeaderText(headerMessage);
        alert.setContentText(infoMessage);
        alert.showAndWait();
    }

}


