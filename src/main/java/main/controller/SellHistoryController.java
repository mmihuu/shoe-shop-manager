package main.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import main.shoe.SoldProduct;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;

public class SellHistoryController implements Initializable {


    @FXML
    public Label sellInThisWeek;
    @FXML
    public Label sellInThisMonth;
    @FXML
    public Label sellThisYear;
    @FXML
    public Slider slder;
    @FXML
    public VBox leftVboxTrial;
    @FXML
    public Button backInTime;
    @FXML
    public AnchorPane mainWindow;
    @FXML
    public VBox maindisplay;


    public void sliderOnA(MouseEvent mouseEvent) throws IOException {


    }

    public void goBackOnA(ActionEvent actionEvent) throws IOException {

        Parent pane = null;

        try {
            pane = FXMLLoader.load(
                    getClass().getResource("/fxml/mainwindow.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        mainWindow.getChildren().clear();
        mainWindow.getChildren().addAll(pane);


    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }


    public void textFieldLookA(ActionEvent actionEvent) {


    }


    public void ExportDBA(ActionEvent actionEvent) {
    }

    public void PrintSellReportA(ActionEvent actionEvent) {
    }

    public void PrintWarehouseA(ActionEvent actionEvent) {
    }

    public void closeOnA(ActionEvent actionEvent) {
    }

    public void deleteFromDBA(ActionEvent actionEvent) {
    }

    public void changeLookA(ActionEvent actionEvent) {
    }

    public void aboutPA(ActionEvent actionEvent) {


    }

    //method to setLabels value of sales

    private void setValueOfSalesForLabels(List<SoldProduct> listPairSoldShoes) {

        BigDecimal oneWeek = BigDecimal.ZERO;
        BigDecimal oneMonth = BigDecimal.ZERO;
        BigDecimal oneYear = BigDecimal.ZERO;
        int a = 0;

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        for (int i = 0; i < listPairSoldShoes.size(); i++) {


            LocalDateTime l = LocalDateTime.now();
            LocalDateTime dateSale = LocalDateTime.parse(listPairSoldShoes.get(i).getLocalDateTime().toString(), dateTimeFormatter);


            if (l.minusDays(7).isBefore(dateSale)) {
                oneWeek = oneWeek.add(new BigDecimal(listPairSoldShoes.get(i).getPrice()).setScale(2, BigDecimal.ROUND_HALF_UP));
            }

            if (l.minusMonths(1).isBefore(dateSale)) {
                oneMonth = oneMonth.add(new BigDecimal(listPairSoldShoes.get(i).getPrice()).setScale(2, BigDecimal.ROUND_HALF_UP));

            }
            if (l.minusYears(1).isBefore(dateSale)) {
                oneYear = oneYear.add(new BigDecimal(listPairSoldShoes.get(i).getPrice()).setScale(2, BigDecimal.ROUND_HALF_UP));
            }


        }



        sellInThisWeek.setText(oneWeek.toString() + " zł");
        sellInThisMonth.setText(oneMonth.toString() + " zł");
        sellThisYear.setText(oneYear.toString() + " zł");




    }


}
