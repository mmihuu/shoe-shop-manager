package main.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import main.controller.report.GenerateReport;
import main.shoe.ProductService;
import main.shoe.SoldProduct;

import java.io.IOException;

public class ChartMainController {

    @FXML
    public AnchorPane mainWindow;
    @FXML
    public HBox hbox;
    @FXML
    public Button lineChart;
    @FXML
    public ScrollPane scrollPane;
    @FXML
    public VBox maindisplay;
    @FXML
    public Slider slider;
    @FXML
    public Button goBack;


    private static int dateRange;

    CategoryAxis categoryAxis;

    public Button barChart;

    public void lineChartOnA(ActionEvent actionEvent) {


        maindisplay.getChildren().clear();
        GenerateReport g = new GenerateReport();

        ObservableList<SoldProduct> soldProducts = ProductService.getSoldProductsBetweenDates(dateRange);

        maindisplay.getChildren().addAll(g.getLineChart(soldProducts));









    }


    public void barChartOnA(ActionEvent actionEvent) {

        maindisplay.getChildren().clear();

        GenerateReport g = new GenerateReport();

        maindisplay.getChildren().addAll(g.getBarChart(ProductService.getSoldProductsBetweenDates(dateRange)));





    }


    public void sliderOnA(MouseEvent mouseEvent) {

        slider.valueProperty().addListener(
                (observable, oldvalue, newvalue) ->
                {
                    int i = newvalue.intValue();

                    dateRange = i;

                });


    }

    public void goBackOnA(ActionEvent actionEvent) {

        Parent pane = null;

        try {
            pane = FXMLLoader.load(
                    getClass().getResource("/fxml/mainwindow.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        mainWindow.getChildren().clear();
        mainWindow.getChildren().add(pane);


    }
}

