package main.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import main.shoe.ProductService;
import main.tools.FormatCell;
import main.tools.InvertoryDoPDF;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;


public class MainWindow  {





     private static double height = 0.0;
    @FXML
    public Label typ;
    @FXML
    public Label kolekcja;
    @FXML
    public Label rozmiar;
    @FXML
    public Label kolor;
    @FXML
    public VBox maindisplay;
    @FXML
    public ScrollPane scrollPane;
    @FXML
    public HBox hBoxClass;
    @FXML
    public ImageView imgClass;
    @FXML
    public Button addShoes;
    @FXML
    public VBox leftVbox;
    @FXML
    public Button historyOfSale;
    @FXML
    public Button reportOfSale;
    @FXML
    public AnchorPane mainWindow;
    @FXML
    public TextField textFieldLook;
    @FXML
    public MenuItem ExportDB;
    @FXML
    public MenuItem PrintSellReport;
    @FXML
    public MenuItem PrintWarehouse;
    @FXML
    public MenuItem close;
    @FXML
    public MenuItem deleteFromDB;
    @FXML
    public MenuItem changeLook;
    @FXML
    public MenuItem aboutP;
    @FXML
    HBox hbox;


    static int dateRange = 1;

    Pane pc;

    private FormatCell setSizeOfCell = new FormatCell();



   /* public void getRecordsFromDB() {
        ReadRecord readRecord = new ReadRecord();
        readRecord.selectAll();

    }*/


    @FXML
    public void initialize() {

        FormatCell formatCell = new FormatCell();
        maindisplay.getChildren().addAll(formatCell.selectAll());
        System.out.println("initi");


    }

    @FXML
    public void addShoesOnAction(ActionEvent actionEvent) throws IOException {

        FXMLLoader loaderFXM = new FXMLLoader();
        loaderFXM.setLocation(getClass().getResource("/fxml/addshoes.fxml"));

        Parent parentView = loaderFXM.load();
        AnchorPane bp = null;

        AddShoesController p = loaderFXM.getController();
        /*p.initDate();*/


        Stage stage = new Stage();


        Scene scene = new Scene(parentView);

        stage.setTitle("Dodaj obuwie do magazynu");


        stage.setScene(scene);
        stage.show();


    }


    public void lookFor(ActionEvent actionEvent) {

        Parent pane = null;
        try {
            pane = FXMLLoader.load(
                    getClass().getResource("/fxml/lookfor.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }


        leftVbox.getChildren().clear();
        leftVbox.getChildren().addAll(pane);


    }

    public void initData(String p, Map<String, String> map) {



        maindisplay.getChildren().clear();
        FormatCell formatCell = new FormatCell();

        maindisplay.getChildren().addAll(formatCell.checkManyRecords(map));


    }
    public void initData(String p) {


        FormatCell formatCell = new FormatCell();
        maindisplay.getChildren().clear();
        System.out.println("tutaj z readTrial");
        List<HBox> hBoxes = formatCell.selectSold(p);



        System.out.println(hBoxes.size()+"wielkość hboxsów");

        maindisplay.getChildren().addAll(hBoxes);







    }

    public void initDataAll() {


    }



    public void selectAll(ActionEvent actionEvent) {

        maindisplay.getChildren().clear();

        FormatCell formatCell = new FormatCell();
        maindisplay.getChildren().addAll(formatCell.selectAll());


    }

    public void reportOfsaleAction(ActionEvent actionEvent) {


        Parent pane = null;
        try {
            pane = FXMLLoader.load(
                    getClass().getResource("/fxml/historytrial.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }


        mainWindow.getChildren().clear();
        mainWindow.getChildren().addAll(pane);



    }

    public void ReportOfSaleOnA(ActionEvent actionEvent) {

        Parent pane = null;
        try {
            pane = FXMLLoader.load(
                    getClass().getResource("/fxml/chartsmain.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }



        mainWindow.getChildren().clear();
        mainWindow.getChildren().addAll(pane);




       /* maindisplay.getChildren().clear();
        GenerateReport generateReport = new GenerateReport();
        maindisplay.getChildren().addAll(generateReport.getLineChart());*/


    }





    public void warehouseOnA(ActionEvent actionEvent) {

        Parent pane = null;
        try {
            pane = FXMLLoader.load(
                    getClass().getResource("/fxml/warehouse.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }



        mainWindow.getChildren().clear();
        mainWindow.getChildren().addAll(pane);



    }

    public void textFieldLookA(ActionEvent actionEvent) {


        maindisplay.getChildren().clear();
        String query = textFieldLook.getText();

       /* ReadRecord readRecord = new ReadRecord();
        readRecord.selectForTextField(query);

        FormatCell formatCell = new FormatCell();

        maindisplay.getChildren().addAll(formatCell.createCells(readRecord.getoList()));*/

       FormatCell formatCell = new FormatCell();
       maindisplay.getChildren().addAll(formatCell.getQueryFromTxBox(ProductService.filterRecords(query)));

        System.out.println(query);
    }



    //methods for the menu
    public void ExportDBA(ActionEvent actionEvent) {



        
    }

    public void PrintSellReportA(ActionEvent actionEvent) {



    }





    public void PrintWarehouseA(ActionEvent actionEvent) throws DocumentException, FileNotFoundException {


        Stage stage = new Stage();


        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));


        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Pdf files", "*.pdf");
        fileChooser.getExtensionFilters().add(extensionFilter);


        File file = fileChooser.showSaveDialog(stage);



        Document doc = null;


        doc = new Document(PageSize.A4);
        PdfWriter writer3 = PdfWriter.getInstance(doc, new FileOutputStream(
                file));

        doc.open();


        InvertoryDoPDF.addTitlePage(doc, "Magazyn", LocalDate.now());
        /*InvertoryDoPDF.addContent(doc, readRecord.getoList(),readRecord.getListOfSizes(),readRecord.countByTheSize());*/



        doc.close();




    }

    public void closeOnA(ActionEvent actionEvent) {

        System.exit(0);

    }

    public void deleteFromDBA(ActionEvent actionEvent) {
    }

    public void changeLookA(ActionEvent actionEvent) {
    }

    public void aboutPA(ActionEvent actionEvent) {

    }



    // próba z wszystkim w tym samym kontrolerze

    public void readTrial(String timeStamp){



        FormatCell formatCell = new FormatCell();
        maindisplay.getChildren().clear();
        System.out.println("tutaj z readTrial");
        List<HBox> hBoxes = formatCell.selectSold(timeStamp);
        System.out.println(hBoxes.size()+"wielkość hboxsów");
        maindisplay.getChildren().addAll(new Button());


    }



    //proba te same

    public void backInTimeOnAction(ActionEvent actionEvent) {




    }

    public void goBackOnA(ActionEvent actionEvent) {
        System.out.println("hello");


    }

    public void sliderOnA(MouseEvent mouseEvent) {


    }
}
