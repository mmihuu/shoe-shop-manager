package main.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import main.shoe.ProductService;
import main.tools.FormatCell;

import java.io.IOException;

public class WarehouseController {

    @FXML
    public AnchorPane wareHouseWindow;
    @FXML
    public HBox hbox;
    @FXML
    public VBox maindisplayware;
    @FXML
    public ScrollPane scrollPane;
    @FXML
    public Button goBack;
    @FXML
    public RadioButton carboard;


    public void goBackOnA(ActionEvent actionEvent) {

        Parent pane = null;

        try {
            pane = FXMLLoader.load(
                    getClass().getResource("/fxml/mainwindow.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        wareHouseWindow.getChildren().clear();
        wareHouseWindow.getChildren().addAll(pane);

    }

    //TODO refactor this for derby and
    public void carboardOnA(ActionEvent actionEvent) {


        /*ReadRecord readRecord = new ReadRecord();
        readRecord.selectGroupBy();


        FormatCell formatCell = new FormatCell();

        maindisplayware.getChildren().addAll(formatCell.createCellsForWare(readRecord.getoList(), readRecord.getListOfSizes(),readRecord.countByTheSize()));*/

        maindisplayware.getChildren().addAll(new FormatCell().createCellsForWare(ProductService.getSoldProductsGroupBy(),ProductService.getListOfSizes(),ProductService.countByTheSize()));

    }
}
