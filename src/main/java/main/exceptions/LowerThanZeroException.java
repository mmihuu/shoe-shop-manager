package main.exceptions;

public class LowerThanZeroException extends Exception {
    public LowerThanZeroException(){
        super("Value has to be greater than zero");

    }

}
