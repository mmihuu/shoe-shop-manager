package main.shoe;


import org.hibernate.annotations.Formula;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name=("WAREHOUSE"))
public class CountByTheSize {


    @Column(name="TYPE_OF_SHOES")
    private String typeOfShoes;
    @Column(name="SIZE_OF_SHOES")
    private int sizeOfShoes;
    @Column(name="COUNT(SIZE_OF_SHOES)")
    private int count;


    public CountByTheSize(String typeOfShoes, int count, int sizeOfShoes) {
        this.typeOfShoes = typeOfShoes;
        this.sizeOfShoes = sizeOfShoes;
        this.count = count;
    }


    public String getTypeOfShoes() {
        return typeOfShoes;
    }

    public void setTypeOfShoes(String typeOfShoes) {
        this.typeOfShoes = typeOfShoes;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getSizeOfShoes() {
        return sizeOfShoes;
    }

    public void setSizeOfShoes(int sizeOfShoes) {
        this.sizeOfShoes = sizeOfShoes;
    }

    @Override
    public String toString() {
        return "CountByTheSize{" +
                "typeOfShoes='" + typeOfShoes + '\'' +
                ", sizeOfShoes=" + sizeOfShoes +
                ", count=" + count +
                '}';
    }
}
