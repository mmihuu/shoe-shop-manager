package main.shoe;

import java.math.BigDecimal;

public abstract class Item {

    abstract BigDecimal buySellRatio(int priceOfBuy, int priceOfSell);
    abstract BigDecimal priceOfBuy();
    abstract BigDecimal priceOfSell();
    abstract BigDecimal profit();
    abstract boolean isAvailable();



}
