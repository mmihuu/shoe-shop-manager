package main.shoe;


import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name="SELL_HISTORY")
public class SoldProduct {


    @Transient
    private Product product;
    @Transient
    private int howManyShoesOfAGivenType;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "TYPE_OF_SHOES")
    private String typeOfShoes;
    @Column(name ="NAME_OF_SHOES")
    private String nameOfShoes;
    @Column(name ="COLOR_OF_SHOES")
    private String colorOfShoes;
    @Column(name="SIZE_OF_SHOES")
    private int sizeOfShoes;
    @Column(name="COLLECTION_OF_SHOES")
    private String collectionOfShoes;
    @Lob
    @Column(name="IMAGE")
    private byte[] imgOfShoes;
    @Column(name="PRICE")
    private float price;


    @Column(name="COLUMN1")
    @CreationTimestamp
    private LocalDateTime localDateTime;

    public SoldProduct(){

    }

    public SoldProduct(Product product) {
        this.product = product;

        this.typeOfShoes = product.getTypeOfShoes();
        this.nameOfShoes = product.getNameOfShoes();
        this.colorOfShoes = product.getColorOfShoes();
        this.sizeOfShoes = product.getSizeOfShoes();
        this.collectionOfShoes = product.getCollectionOfShoes();
        this.imgOfShoes = product.getImgOfShoes();
        this.price = product.getPrice();
    }



    public LocalDate getLocalDate(LocalDateTime dateToConvert) {
        return dateToConvert.toLocalDate();
    }

    public String getDay(){

        return localDateTime.toString().substring(0,10);
    }




    public LocalDateTime getLocalDateTime() {

        return localDateTime;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeOfShoes() {
        return typeOfShoes;
    }

    public void setTypeOfShoes(String typeOfShoes) {
        this.typeOfShoes = typeOfShoes;
    }

    public String getNameOfShoes() {
        return nameOfShoes;
    }

    public void setNameOfShoes(String nameOfShoes) {
        this.nameOfShoes = nameOfShoes;
    }

    public String getColorOfShoes() {
        return colorOfShoes;
    }

    public void setColorOfShoes(String colorOfShoes) {
        this.colorOfShoes = colorOfShoes;
    }

    public int getSizeOfShoes() {
        return sizeOfShoes;
    }

    public void setSizeOfShoes(int sizeOfShoes) {
        this.sizeOfShoes = sizeOfShoes;
    }

    public String getCollectionOfShoes() {
        return collectionOfShoes;
    }

    public void setCollectionOfShoes(String collectionOfShoes) {
        this.collectionOfShoes = collectionOfShoes;
    }

    public byte[] getImgOfShoes() {
        return imgOfShoes;
    }

    public void setImgOfShoes(byte[] imgOfShoes) {
        this.imgOfShoes = imgOfShoes;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getHowManyShoesOfAGivenType() {
        return howManyShoesOfAGivenType;
    }

    public void setHowManyShoesOfAGivenType(int howManyShoesOfAGivenType) {
        this.howManyShoesOfAGivenType = howManyShoesOfAGivenType;
    }
}
