package main.shoe;

public class ListOfSizes {

    //tutaj będzie lista z wszystkimi modelami
    String nameOfShoes;
    int sizeOfShoes;

    public ListOfSizes(String nameOfShoes, int sizeOfShoes) {
        this.nameOfShoes = nameOfShoes;
        this.sizeOfShoes = sizeOfShoes;
    }

    public String getNameOfShoes() {
        return nameOfShoes;
    }

    public void setNameOfShoes(String nameOfShoes) {
        this.nameOfShoes = nameOfShoes;
    }

    public int getSizeOfShoes() {
        return sizeOfShoes;
    }

    public void setSizeOfShoes(int sizeOfShoes) {
        this.sizeOfShoes = sizeOfShoes;
    }

    @Override
    public String toString() {
        return "ListOfSizes{" +
                "nameOfShoes='" + nameOfShoes + '\'' +
                ", sizeOfShoes=" + sizeOfShoes +
                '}';
    }
}
