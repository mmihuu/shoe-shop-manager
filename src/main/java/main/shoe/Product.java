package main.shoe;


import javafx.scene.image.Image;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Date;
import java.time.LocalDateTime;

@Entity
/*@FilterDef(name="TYPE_OF_SHOES", parameters=@ParamDef(name=))
@Filter(name = "type", condition = "")*/
@Table(name="WAREHOUSE")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "TYPE_OF_SHOES")
    private String typeOfShoes;
    @Column(name ="NAME_OF_SHOES")
    private String nameOfShoes;
    @Column(name ="COLOR_OF_SHOES")
    private String colorOfShoes;
    @Column(name="SIZE_OF_SHOES")
    private int sizeOfShoes;
    @Column(name="COLLECTION_OF_SHOES")
    private String collectionOfShoes;
    @Lob
    @Column(name="IMAGE")
    private byte[] imgOfShoes;
    @Column(name="PRICE")
    private float price;

    @CreationTimestamp
    private LocalDateTime COLUMN1;


/*    @Formula(value ="COUNT(*)")
    private int countOfShoes;*/
   /* @Formula(value ="SUM(PRICE)")
    private float sumOfPrice;*/





    public Product(){

    }
    private Product(Builder builder){

        this.typeOfShoes = builder.typeOfShoes;
        this.nameOfShoes = builder.nameOfShoes;
        this.colorOfShoes = builder.colorOfShoes;
        this.sizeOfShoes = builder.sizeOfShoes;
        this.imgOfShoes = builder.imgOfShoes;
        this.collectionOfShoes = builder.collectionOfShoes;
        this.price =builder.price;
    }




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeOfShoes() {
        return typeOfShoes;
    }

    public void setTypeOfShoes(String typeOfShoes) {
        this.typeOfShoes = typeOfShoes;
    }

    public String getNameOfShoes() {
        return nameOfShoes;
    }

    public void setNameOfShoes(String nameOfShoes) {
        this.nameOfShoes = nameOfShoes;
    }

    public String getColorOfShoes() {
        return colorOfShoes;
    }

    public void setColorOfShoes(String colorOfShoes) {
        this.colorOfShoes = colorOfShoes;
    }

    public int getSizeOfShoes() {
        return sizeOfShoes;
    }

    public void setSizeOfShoes(int sizeOfShoes) {
        this.sizeOfShoes = sizeOfShoes;
    }


    public String getCollectionOfShoes() {
        return collectionOfShoes;
    }

    public void setCollectionOfShoes(String collectionOfShoes) {
        this.collectionOfShoes = collectionOfShoes;
    }

    public byte[] getImgOfShoes() {
        return imgOfShoes;
    }

    public void setImgOfShoes(byte[] imgOfShoes) {
        this.imgOfShoes = imgOfShoes;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    /*public int getCountOfShoes() {
        return countOfShoes;
    }*/

  /*  public float getSumOfPrice() {
        return sumOfPrice;
    }
*/

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", typeOfShoes='" + typeOfShoes + '\'' +
                ", nameOfShoes='" + nameOfShoes + '\'' +
                ", colorOfShoes='" + colorOfShoes + '\'' +
                '}';
    }

    public static class Builder {

        private String typeOfShoes;
        private String nameOfShoes;
        private String colorOfShoes;
        private int sizeOfShoes;
        private String collectionOfShoes;
        private byte[] imgOfShoes;
        private float price;
        private String date;
        /*private LocalDateTime localDateTime;*/
        int ID;


        public Builder() {

        }

        public Builder date(final String date) {

            this.date = date;
            return this;
        }

        /*public Builder localDateTime(final LocalDateTime localDateTime) {
            this.localDateTime = localDateTime;
            return this;

        }*/

        public Builder typeOfShoes(final String typeOfShoes) {
            this.typeOfShoes = typeOfShoes;
            return this;
        }

        public Builder nameOfShoes(final String nameOfShoes) {
            this.nameOfShoes = nameOfShoes;
            return this;
        }

        public Builder colorOfShoes(final String colorOfShoes) {
            this.colorOfShoes = colorOfShoes;
            return this;
        }

        public Builder sizeOfShoes(final int sizeOfShoes) {
            this.sizeOfShoes = sizeOfShoes;
            return this;
        }

        public Builder collectionOfShoes(final String collectionOfShoes) {
            this.collectionOfShoes = collectionOfShoes;
            return this;
        }

        public Builder imgOfShoes(final byte[] imgOfShoes) {
            this.imgOfShoes = imgOfShoes;
            return this;
        }

        public Builder priceOfShoes(final float price) {
            this.price = price;
            return this;
        }

        /*public Builder id(int id) {
            this.ID = id;
            return this;
        }*/

        public Product build() {
            return new Product(this);
        }


    }
}
