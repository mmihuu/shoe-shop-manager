package main.shoe;

import java.math.BigDecimal;

public class BuySellRatio extends Item {

    private BigDecimal priceOfBuy;
    private BigDecimal priceOfSell;
    private BigDecimal ratio;
    private BigDecimal profit;


    @Override
    BigDecimal buySellRatio(int priceOfBuy, int priceOfSell) {
        return null;
    }

    @Override
    BigDecimal priceOfBuy() {
        return null;
    }

    @Override
    BigDecimal priceOfSell() {
        return null;
    }

    @Override
    BigDecimal profit() {
        return null;
    }

    @Override
    boolean isAvailable() {
        return false;
    }


    //getters and setters


    public BigDecimal getPriceOfBuy() {
        return priceOfBuy;
    }

    public void setPriceOfBuy(BigDecimal priceOfBuy) {
        this.priceOfBuy = priceOfBuy;
    }

    public BigDecimal getPriceOfSell() {
        return priceOfSell;
    }

    public void setPriceOfSell(BigDecimal priceOfSell) {
        this.priceOfSell = priceOfSell;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }
}
