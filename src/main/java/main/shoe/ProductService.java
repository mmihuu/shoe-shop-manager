package main.shoe;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.tools.HibernateUtil;
import org.hibernate.Session;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ProductService {

    private ProductService() {

    }

    public static Product getProductByID(Integer id) {

        Product product;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            product = session.get(Product.class, id);

        }
        return product;
    }


    public static void save(Product product) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(product);
            session.getTransaction().commit();
        }


    }

    public static ObservableList<Product> getProducts() {
        ObservableList<Product> listProduct = FXCollections.observableArrayList();


        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List from_product = session.createQuery("from Product").list();
            listProduct.addAll(from_product);
        }


        return listProduct;
    }

    public static void removeById(Integer Id) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            Product itemToDelete = session.load(Product.class, Id);
            session.delete(itemToDelete);
            session.getTransaction().commit();


        }


    }

    public static void insertToSellTable(SoldProduct soldProduct) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.beginTransaction();
            session.save(soldProduct);
            session.getTransaction().commit();


        }


    }

    public static ObservableList<Product> filterRecords(String query) {

        System.out.println(query);

        ObservableList<Product> observableList = FXCollections.observableArrayList();


        List fromProduct;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            if (query.matches(".*\\d.*")) {

                fromProduct = session.createQuery("from Product where SIZE_OF_SHOES =" + query + " or PRICE =" + query).list();

                observableList.addAll(fromProduct);

            } else {
                fromProduct = session.createQuery("from Product where NAME_OF_SHOES like '%" + query + "%'"
                        + " or TYPE_OF_SHOES like '%" + query + "%'").list();

                observableList.addAll(fromProduct);
            }
        }


        return observableList;

    }

    public static ObservableList<Product> checkMultipleQueries(Map<String, String> mapToDB) {

        ObservableList observableList = FXCollections.observableArrayList();

        Set<Map.Entry<String, String>> entries = mapToDB.entrySet();
        System.out.println(entries);
        StringBuilder whereClause = new StringBuilder();

        int counter = 1;
        for (Map.Entry<String, String> e : entries) {

            whereClause.append(e.getKey() + " = " + e.getValue());
            if (entries.size() > counter) {
                whereClause.append(" AND ");
                counter++;
            }

        }


        String sql = "FROM Product WHERE " + whereClause;
        System.out.println(sql);

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            List list = session.createQuery(sql).list();
            observableList.addAll(list);


        }

        return observableList;

    }

    public static ObservableList<SoldProduct> getSoldProductsBetweenDates(int howManyMonthsBack) {

        ObservableList<SoldProduct> listSoldProduct = FXCollections.observableArrayList();
        System.out.println("timeback " + howManyMonthsBack);
        LocalDateTime localDateTime = LocalDateTime.now();
        localDateTime = localDateTime.minusMonths(howManyMonthsBack + 1);
        Timestamp timestamp = Timestamp.valueOf(localDateTime);


        try (Session session = HibernateUtil.getSessionFactory().openSession()) {


            List list = session.createQuery("FROM SoldProduct WHERE COLUMN1 BETWEEN '" + timestamp + "'AND CURRENT_TIMESTAMP").list();


            listSoldProduct.addAll(list);


        }


        return listSoldProduct;

    }


    public static ObservableList<Product> getSoldProductsGroupBy() {

        ObservableList<Product> listSoldProduct = FXCollections.observableArrayList();


        try (Session session = HibernateUtil.getSessionFactory().openSession()) {


            List list = session.createQuery("SELECT SUM(PRICE), COUNT(NAME_OF_SHOES) FROM  Product GROUP BY NAME_OF_SHOES").list();


            listSoldProduct.addAll(list);


        }
        return listSoldProduct;


    }

    //TODO refactor it !! two auxiliary methods for warehouse

    public static List<ListOfSizes> getListOfSizes(){

        List<ListOfSizes> listOfSizes = new ArrayList<>();
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {


            List list = session.createQuery("SELECT NAME_OF_SHOES, SIZE_OF_SHOES FROM Product GROUP BY NAME_OF_SHOES, SIZE_OF_SHOES").list();


            listOfSizes.addAll(list);


        }


        return listOfSizes;

    }
    public static List<CountByTheSize> countByTheSize(){

        List<CountByTheSize> listOfSizes = new ArrayList<>();
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {


            List list = session.createQuery("SELECT NAME_OF_SHOES, SIZE_OF_SHOES, count(SIZE_OF_SHOES) FROM CountByTheSize GROUP BY NAME_OF_SHOES, SIZE_OF_SHOES").list();


            listOfSizes.addAll(list);


        }


        return listOfSizes;





    }




}
